## Commands

There are many commands available to help you build and test sites. Here are a few highlights to get started with.

### Watch For Changes & Automatically Refresh Across Devices

## Build & Optimize

```sh
$ gulp
```

Build and optimize the current project, ready for deployment.
This includes linting as well as image, script, stylesheet and HTML optimization and minification.
Also, a [browsersync](https://browsersync.io/) script will be automatically generated, which will take care of precaching your sites' resources.


```sh
$ gulp dev
```

Same as 'gulp' command but without starting the local server.


## Serve the Fully Built & Optimized Site

```sh
$ gulp production
```

`gulp production` task creates the `production/` folder in the root of the project with **assets files only**. It will **help you** to **create clear** instances of code for the **production** or **further implementation**.


## Structure

Your folder structure for WSK:
If you want to use our WSK , you need to know something about the structure.

```
├── assets          #Folder with files after compiling
├── src             #Folder with sources
├── tasks           #Folder with tasks for gulpfile
├── LICENSE
├── README.md
├── gulp-config.js  #Config for gulp
├── gulpfile.js     #File with gulp tasks
├── index.html      #Main application
└── package.json    #File with dependencies

```

`Tasks` - folder for gulpfile tasks.
In `package.json` you can find all the dependencies.
In `src` folder you can find all sources for the project (images, sass , javascript files).

## `src` folder structure

```
├── images                      #Folder for storing images
├── js                          #Folder for storing js files
   ├── modules                  #Folder for storing js modules
   ├── app.js                   #Main js file
├── scss
   ├── abstracts                #Folder for storing scss files
      ├── _functions.scss       #Sass functions
      ├── _helpers.scss         #Sass helpers
      ├── _mixins.scss          #Sass mixins
      ├── _variables.scss       #Sass variables that we can use in our scss files
   ├── base                     #Folder for storing base styles
      ├── _forms.scss           #Sass styles for forms      
      ├── _main.scss            #Main scss file for base styles      
      ├── _reset.scss           #Sass reset
      ├── _typography.scss      #Sass styles for text      
   ├── components               #Global Reusable Presentational Components
   ├── layout                   #Global layout
   ├── pages                    #Global styles for pages
   ├── style.scss               #Main scss file (can be used for importing another files)
#This `templates` folder will be created if you choose a Pug option using jcn
├── templates                   #Folder with pug templates
   ├── layouts                  #Folder with pug layouts
      ├── default.pug           #Example of the pug layout
   ├── mixins                   #Folder with pug mixins
      ├── article.pug           #Example of the pug mixin
   ├── views                    #Folder with pug pages
      ├── blog.pug              #Example of a blog page
      ├── index.pug             #Example of a index page
├── vendor_entries              #Folder for vendor entries(plugins)
  ├── vendor.js                 #File for plugins js 
  ├── vendor.scss               #File for plugins styles

```

Use `images` folder to add your graphic files, `modules` to add your javascript modules (don't forget to include it in app.js), `scss` folder to add your styles for the project. You can create, delete files and folders in `scss`, but don't forget to include them in `style.scss` file .

Use `vendor_entries` to include plugins into your project.

## JS

 In our WSK you can use ES2015(ES6). ES2015 isn't introducing anything other than improvements to the JavaScript language and a few new features. 

 It is not an alternative syntax or language like CoffeeScript or TypeScript. It's good ol' fashioned JavaScript. The reason so many people are excited is that this version introduces a lot of much-needed improvements to the language. 

* All custom **javascript** files are located in `js/` folder;
* Entry point for javascript is `src/js/app.js` you can **import** all your *.js* files from here using [ES6 import](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import) feature;
* All javascript is **babelified** so yes! You can use all kind of [ES6 features](https://babeljs.io/docs/learn-es2015/) here.
* All **extensions** must be installed by the [NPM](https://docs.npmjs.com/cli/install);
* After installing the extension you must **include its files**:
  * **js files** must be included in `src/vendor_entries/vendor.js` by adding new elements to the **array**.



## SCSS

In our WSK you can use [SASS](http://sass-lang.com/). Sass is the most mature, stable, and powerful professional grade CSS extension language in the world.

Sass is a CSS preprocessor — a layer between the stylesheets you author and the .css files you serve to the browser. Sass (short for Syntactically Awesome Stylesheets) plugs the holes in CSS as a language, allowing you to write DRY code that’ll be faster, more efficient, and easier to maintain. In our WSK we follow Sass [guidelines](https://sass-guidelin.es/#architecture).

So while normal CSS doesn’t yet allow things like variables, mixins (reusable blocks of styles), and other goodies, Sass provides a syntax that does all of that and more—enabling “super functionality” in addition to your normal CSS.  

* All custom **scss** files locate in `src/scss/` folder;
* Entry point for all scss is `src/scss/style.scss` you can **import** all your *.scss* files from here;
* You **don't need** to write **prefixes** for different browsers like `-webkit` it will be done by the gulp.

The `src` directory above contains MDL's Sass files and the JavaScript sources for all MDL components.

* All **extensions** must be installed by the [NPM](https://docs.npmjs.com/cli/install);
* After installing the extension you must **include its files**:
  * **css or sass files** must be included in `src/vendor_entries/vendor.scss` using `@import`.
  
You are able to add your own custom sass files and optionally [disable/enable gulp-group-css-media-queries module](https://github.com/justcoded/web-starter-kit/issues/56).
You can see this property in the gulp-config.js file:

![image](https://user-images.githubusercontent.com/14921077/36471272-8ce84fbc-16f6-11e8-8173-4e41d1dbbb9f.png)

Please don't forget to link all your custom scss files in html:

![image](https://user-images.githubusercontent.com/14921077/36471279-9387a368-16f6-11e8-9814-a7f1a18150fc.png)

Also, you might want to add these sass files to the ignore list (check `getPathesToCopyForProduction` and `getPathesToCopy` properties in the `gulp-config.js` file). By default they will be copied to the assets and production folders.

## Tasks

|Task                               | Description                                                                                                                                                                                                                                               |
|----------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| browser-sync-server | Browsersync can watch your files as you work. Changes you make will either be injected into the page (CSS & images) or will cause all browsers to do a full-page refresh.                      |
| build-custom-js                         | Compiles all custom js from `src/js`.                                                                                                      |
| build-js-vendors               | minifies and сompiles all vendor js from `src/vendor_entries`.                                                                                                |
| build-sass-production               | Compiles and minifies all custom scss from `src/scss` to `production`   folder.                                                                                               |
| build-sass                   | Compiles all custom scss from `src/scss` to `assets/css`   folder.  |
| build-styles-vendors                 | Compiles and minifies all plugins  scss from `src/vendor_entries` to `production`   folder.                                                                                                                                                                            |
| clean-production                | `production` folder removing.                                                                                                                           |
| copy-folders           | Need to copy all folders from sources to assets.                       |
| templates           | Compiles all pug files into html files.                       |
| html-hint           | Need to hint html files.                    |
| js-hint           | Need to hint js files.                      |
| image-clean           | Removing images.                    |
| image-min           | We use this to minify images.                    |
| watch           | Task for watching all the changes.                   |
| compile-sass-files           | Compiles all custom scss files listed in the `gulp-config.js` to `assets/css`   folder                   |

## Troubleshooting

If you find yourself running into issues during installation or running the tools, please check our [Troubleshooting](https://github.com/justcoded/web-starter-kit/wiki/Troubleshooting) guide and then open an [issue](https://github.com/justcoded/web-starter-kit/issues). We would be happy to discuss how they can be solved.


## Contributing

Contributions, questions and comments are all welcome and encouraged. For code contributions to Web Starter Kit, please see our [Contribution guide](CONTRIBUTING.md) before submitting a pull request. [Website](https://github.com/justcoded/web-starter-kit) related issues should be filed on the [Web Fundamentals](https://github.com/justcoded/web-starter-kit/issues) issue tracker.

## License

The MIT License (MIT).

Copyright (c) 2018 JustCoded the IP provided on other devices on your network.
                                                                                                                                     
